# FreeCAD-VDI-Importer

Prototype to import VDI file to FreeCAD.

### Installation

Go to the `FreeCAD/Mod` directory and clone this repository 
(`git clone https://gitlab.com/amrit3701/freecad-vdi-importer.git`).

### Usage

In the FreeCAD Python console run below commands:
```python
import vdi_importer
vdi_importer.import_file(<vdi_file_path>)  # If you not pass vdi file path, then importer imports sample vdi file.
```

### How .vdi file looks like?

Here is the [example.vdi](https://gitlab.com/amrit3701/freecad-vdi-importer/-/blob/master/tests/example_file.vdi) file.

```
#1=PROJECT_NAME;Test example for VDI importer
#2=CUBOID;200;200;200;#3;#4
#3=POSITION;-100;-100;100
#4=ROTATION;0;0;1;0
#5=CYLINDER;400;40;#6;#7
#6=POSITION;0;0;0
#7=ROTATION;0;0;1;0
#8=ADD;#2;#5
#9=CYLINDER;400;40;#10;#11
#10=POSITION;-200;0;200
#11=ROTATION;0;1;0;90
#12=ADD;#8;#9
```

**Output**

![Example Output Image](_static/example_output.png)