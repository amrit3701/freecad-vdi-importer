import pathlib

from .constants import (
    EQUAL_OPERATOR,
    SPLIT_OPERATOR,
    BooleanOperations,
    Primitives,
    OtherEntities,
)
from .freecad_utils import create_new_fc_doc
from .handlers import primitive_handler, boolean_operation_handler


_DIR_PATH = pathlib.Path(__file__).parent.parent


def import_file(file_path: str = None, new_doc: bool = True):
    """
    Import .vdi file.

    If `new_doc` is True, then importer import a file into new document, else
    import it into active document.
    """

    if file_path is None:
        file_path = _DIR_PATH / "tests" / "example_file.vdi"
    else:
        file_path = pathlib.Path(file_path)
        # TODO Add more validations.
        if not file_path.is_file():
            raise FileNotFoundError(f"Given {file_path} is not present.")

    data = {}

    new_doc and create_new_fc_doc()

    for line in open(file_path).readlines():
        key, value = line.split(EQUAL_OPERATOR)
        data[key] = value.rstrip("\n").split(SPLIT_OPERATOR)

    for key, values in data.items():
        entity_name, *v = values

        if Primitives.has_value(entity_name):
            data[key].append(primitive_handler(data, entity_name, v))
        elif BooleanOperations.has_value(entity_name):
            data[key].append(boolean_operation_handler(data, entity_name, v))
        elif OtherEntities.has_value(entity_name):
            pass
        else:
            raise NotImplementedError(
                f"{entity_name} entity is not implemented yet."
            )
