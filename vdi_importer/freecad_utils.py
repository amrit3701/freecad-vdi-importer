from math import radians

import FreeCAD


def create_new_fc_doc(name: str = "Document"):
    """Create new FreeCAD document of given name."""
    doc = FreeCAD.newDocument(name)
    FreeCAD.setActiveDocument(doc.Name)
    FreeCAD.ActiveDocument = FreeCAD.getDocument(doc.Name)
    return doc


def create_cuboid(length, width, height, pl_base, pl_rot):
    """Create FreeCAD box obj."""
    box = FreeCAD.ActiveDocument.addObject("Part::Box", "Box")
    box.Length = float(length)
    box.Width = float(width)
    box.Height = float(height)
    box.Placement.Base = FreeCAD.Vector(pl_base)
    box.Placement.Rotation.Axis = FreeCAD.Vector(
        pl_rot[0], pl_rot[1], pl_rot[2]
    )
    box.Placement.Rotation.Angle = radians(pl_rot[3])
    FreeCAD.ActiveDocument.recompute()
    return box


def create_cylinder(height, radius, pl_base, pl_rot):
    """Create FreeCAD cylinder obj."""
    c = FreeCAD.ActiveDocument.addObject("Part::Cylinder", "Cylinder")
    c.Height = float(height)
    c.Radius = float(radius)
    c.Placement.Base = FreeCAD.Vector(pl_base)
    c.Placement.Rotation.Axis = FreeCAD.Vector(pl_rot[0], pl_rot[1], pl_rot[2])
    c.Placement.Rotation.Angle = radians(pl_rot[3])
    FreeCAD.ActiveDocument.recompute()
    return c


def union(*shapes):
    """Create union object of given shapes."""
    obj = FreeCAD.ActiveDocument.addObject("Part::MultiFuse", "Fusion")
    obj.Shapes = shapes
    FreeCAD.ActiveDocument.recompute()
    return obj
