from .constants import BooleanOperations, Primitives, OtherEntities
from .freecad_utils import create_cuboid, create_cylinder, union


def _get_pl_base(d: list):
    """Get placement base vector."""

    entity_name, *vec = d
    assert (
        entity_name == OtherEntities.position.value
    ), f"Type of {d} should be {OtherEntities.position.value}"
    return [float(i) for i in vec]


def _get_rot(d: list):
    """Get rotation of object."""

    entity_name, *rot_data = d
    assert (
        entity_name == OtherEntities.rotation.value
    ), f"Type of {d} should be {OtherEntities.rotation.value}"
    return [float(i) for i in rot_data]


def primitive_handler(data: dict, entity_name: str, v: list):
    """Handler to create all primitive entities."""

    assert Primitives.has_value(
        entity_name
    ), f"{entity_name} primitive entity is not implemented yet."

    if entity_name == Primitives.cuboid.value:
        obj = create_cuboid(
            v[0], v[1], v[2], _get_pl_base(data[v[3]]), _get_rot(data[v[4]])
        )
    else:
        obj = create_cylinder(
            v[0], v[1], _get_pl_base(data[v[2]]), _get_rot(data[v[3]])
        )

    return obj


def boolean_operation_handler(data: dict, operation: str, v: list):
    """Handler to handle all boolean operations."""

    assert BooleanOperations.has_value(
        operation
    ), f"{operation} boolean operation is not implemented yet."

    obj = union(data[v[0]][-1], data[v[1]][-1])
    return obj
