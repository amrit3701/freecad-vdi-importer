from enum import Enum


EQUAL_OPERATOR = "="
SPLIT_OPERATOR = ";"


class BaseEnum(Enum):
    @classmethod
    def has_value(cls, value) -> bool:
        """Return True, if any enum properties has given value."""
        return value in cls._value2member_map_


class Primitives(BaseEnum):
    """Primitive shapes."""

    cuboid: str = "CUBOID"
    cylinder: str = "CYLINDER"


class BooleanOperations(BaseEnum):
    """Boolean operations"""

    union: str = "ADD"


class OtherEntities(BaseEnum):
    project_name: str = "PROJECT_NAME"
    position: str = "POSITION"
    rotation: str = "ROTATION"
